** getInsights **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param;
    
    if(event.action == "GROUP_INSIGHT") {
        param = {
            TableName: 'Groups',
            KeyConditionExpression: "#name = :name",
            ExpressionAttributeNames:{
                "#name": "name"
            },
            ExpressionAttributeValues: {
                ":name": event.groupName
            }
        };
        
            
        dynamoDB.query(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                callback(null, data);
            }
        });
    } else {
        callback("Invalid action requested", null);
    }
}

** AddNewQuery **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();

exports.handler = function(event, context, callback){
    if(!event.id) {
        event["id"] = context.awsRequestId;
    }
    event["createdOn"] = Date.now();
    
    if(event.imageBase64 != null) {
        var params = {
            "Body": event.imageBase64,
            "Bucket": "loudmind-images",
            "Key": event.authorPhoneNumber + "_" + event.id + ".png"  
        };
        
        s3.upload(params, function(err, data){
            if(err) {
                callback(err, null);
            } else {
                event["imageFileName"] = "loudmind-images/" + event.authorPhoneNumber + "_" + event.id + ".png";  
                event["imageBase64"] = undefined;
                
                let param = {
                    TableName: 'Queries',
                    Item:event
                };
                
                dynamoDB.put(param, function(err, data) {
                    if (err) {
                        callback(err, null);
                    } else {
                        callback(null,event["id"]);
                    }
                });
            }
        });
    } else {
        let param = {
            TableName: 'Queries',
            Item:event
        };
        
        dynamoDB.put(param, function(err, data) {
            if (err) {
                callback(err, null);
            } else {
                callback(null,event["id"]);
            }
        });
    }
};

** UpdateGroupQuery **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {};
    
    if(event.action == "ADD_RESPONSE") {
        event.updatedResponse["createdOn"] = Date.now();
        param = {
            TableName: 'Queries',
            Key:{
                "id": event.id
            },
            UpdateExpression: "set #response = list_append(if_not_exists(#response, :empty_list), :newRequest)",
            ExpressionAttributeNames: {
                '#response' : 'response'
            },
            ExpressionAttributeValues:{
                ":newRequest": [event.updatedResponse],
                ':empty_list': []
            },
            ReturnValues:"UPDATED_NEW"
        };
    } else if(event.action == "UP_VOTE_QUERY") {
        param = {
            TableName: 'Queries',
            Key:{
                "id": event.id
            },
            UpdateExpression: "set #upvotedBy = list_append(if_not_exists(#upvotedBy, :empty_list), :newRequest)",
            ExpressionAttributeNames: {
                '#upvotedBy' : 'upvotedBy'
            },
            ExpressionAttributeValues:{
                ":newRequest": [event.addUpVoteMember],
                ':empty_list': []
            },
            ReturnValues:"UPDATED_NEW"
        };
    } else if(event.action == "DOWN_VOTE_QUERY") {
        param = {
            TableName: 'Queries',
            Key:{
                "id": event.id
            },
            UpdateExpression: "set #downvotedBy = list_append(if_not_exists(#downvotedBy, :empty_list), :newRequest)",
            ExpressionAttributeNames: {
                '#downvotedBy' : 'downvotedBy'
            },
            ExpressionAttributeValues:{
                ":newRequest": [event.addDownVoteMember],
                ':empty_list': []
            },
            ReturnValues:"UPDATED_NEW"
        };
    }
    
    dynamoDB.update(param, function(err, data) {
    if (err) {
        callback(err, null);
    } else {
        const response = {
                  statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
              },
            };
            callback(null,response);
    }
});
};

** addGroupMember **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {};
    if(event.phoneNumber) {
        
        // CODE to directly join
        let newMemberRequest =  {
               phoneNumber : event.phoneNumber,
               nickName: event.nickName,
               createdOn: Date.now()
            };
            
        param = {
            TableName: 'Groups',
            Key:{
                "name": event.groupName
            },
            UpdateExpression: "set #members = list_append(if_not_exists(#members, :empty_list), :newMemberRequest)",
            ExpressionAttributeNames: {
                '#members' : 'members'
            },
            ExpressionAttributeValues:{
                ":newMemberRequest": [newMemberRequest],
                ':empty_list': []
            },
            ReturnValues:"UPDATED_NEW"
        };
          
        dynamoDB.update(param, function(err, data) {
            if (err) {
                callback(err, null);
            } else {
                const response = {
                          statusCode: 200,
                      headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                      },
                    };
                    callback(null,response);
            }
        });
            
            // CODE to 1st send request to join with approve/reject
        // let newRequest =  {
        //   phoneNumber: event.phoneNumber,
        //   nickName: event.nickName,
        //   createdOn: Date.now()
        // };

        // param = {
        //     TableName: 'Groups',
        //     Key:{
        //         "name": event.groupName
        //     },
        //     UpdateExpression: "set #pendingAddRequests = list_append(if_not_exists(#pendingAddRequests, :empty_list), :newRequest)",
        //     ExpressionAttributeNames: {
        //         '#pendingAddRequests' : 'pendingAddRequests'
        //     },
        //     ExpressionAttributeValues:{
        //         ":newRequest": [newRequest],
        //         ':empty_list': []
        //     },
        //     ReturnValues:"UPDATED_NEW"
        // };
        
        // dynamoDB.update(param, function(err, data) {
        //     if (err) {
        //         callback(err, null);
        //     } else {
        //         const response = {
        //                   statusCode: 200,
        //               headers: {
        //                 "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
        //                 "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
        //               },
        //             };
        //             callback(null,response);
        //     }
        // });
        
        
    }
    
    if(event.newMember) {
        param = {
           TableName: 'Groups',
            KeyConditionExpression: "#name = :name",
            ExpressionAttributeNames:{
                "#name": "name"
            },
            ExpressionAttributeValues: {
                ":name": event.groupName
            } 
        };
        
        dynamoDB.query(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            let newMemberRequest =  {
               phoneNumber : event.newMember,
               nickName: event.nickName,
               createdOn: Date.now()
            };
            
            if(data["Count"] > 0) {
                let currentPendingRequests = data["Items"][0]["pendingAddRequests"];
                if(currentPendingRequests != null) {
                    let newPendingRequests = [];
                    for(i = 0; i < currentPendingRequests.length; i++) {
                        if(currentPendingRequests[i]["phoneNumber"] != event.newMember) {
                            newPendingRequests.push(currentPendingRequests[i]);
                        }
                    }
                    param = {
                        TableName: 'Groups',
                        Key:{
                            "name": event.groupName
                        },
                        UpdateExpression: "set #members = list_append(if_not_exists(#members, :empty_list), :newMemberRequest), #pendingAddRequests = :newPendingRequests",
                        ExpressionAttributeNames: {
                            '#members' : 'members',
                            '#pendingAddRequests' : 'pendingAddRequests'
                        },
                        ExpressionAttributeValues:{
                            ":newMemberRequest": [newMemberRequest],
                            ':empty_list': [],
                            ':newPendingRequests': newPendingRequests
                        },
                        ReturnValues:"UPDATED_NEW"
                    };
                }
            } else {
                param = {
                    TableName: 'Groups',
                    Key:{
                        "name": event.groupName
                    },
                    UpdateExpression: "set #members = list_append(if_not_exists(#members, :empty_list), :newMemberRequest)",
                    ExpressionAttributeNames: {
                        '#members' : 'members'
                    },
                    ExpressionAttributeValues:{
                        ":newMemberRequest": [newMemberRequest],
                        ':empty_list': []
                    },
                    ReturnValues:"UPDATED_NEW"
                };
            }
            
            dynamoDB.update(param, function(err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    const response = {
                              statusCode: 200,
                          headers: {
                            "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                            "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                          },
                        };
                        callback(null,response);
                }
            });
        }
    });
}
}

** RemoveGroupMember **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    if(event.action == "GET_GROUPS") {
        let param = {
            TableName: 'Groups',
            Limit: 10,
            ScanIndexForward: false,
            ConsistentRead: false,
            ExclusiveStartKey: event.exclusiveStartKey
        };
    
        dynamoDB.scan(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                callback(null,data);
            }
        });
    }
    
    if(event.action == "GET_GROUP") {
        let param = {
            TableName: 'Groups',
            KeyConditionExpression: "#name = :groupName",
            ExpressionAttributeNames:{
                "#name": "name"
            },
            ExpressionAttributeValues: {
                ":groupName": event.groupName
            }
        };
        
        dynamoDB.query(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                callback(null,data);
            }
        });
    }
    
    if(event.action == "DENY_USER_ADD_REQUEST" && event.newMember) {
        let param = {
            TableName: 'Groups',
            KeyConditionExpression: "#name = :groupName",
            ExpressionAttributeNames:{
                "#name": "name"
            },
            ExpressionAttributeValues: {
                ":groupName": event.groupName
            }
        };
        
        dynamoDB.query(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            var pendingRequests = [];
            var group = data["Items"][0];
            if(group["pendingAddRequests"].length > 0) {
               pendingRequests = (group["pendingAddRequests"]).filter((request) => 
                   request["phoneNumber"] != event.newMember
               ); 
               
               if(group["pendingAddRequests"].length != pendingRequests.length) {
                    var param2 = {
                        TableName: 'Groups',
                        Key:{
                            "name": event.groupName
                        },
                        UpdateExpression: "set pendingAddRequests = :pendingRequests",
                        ExpressionAttributeValues:{
                            ":pendingRequests": pendingRequests
                        },
                        ReturnValues:"UPDATED_NEW"
                    };
                        
                    dynamoDB.update(param2, function(err, data) {});
               }
            }
        }
    });
    }
};

** ClearGroupsMember **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {
        TableName: 'Groups'
    };
    
    dynamoDB.scan(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            (data["Items"]).forEach(function(group){
                if(group["pendingAddRequests"].length > 0) {
                    var pendingRequests = (group["pendingAddRequests"]).filter((newRequest) => {
                        var notMatched = true;
                        (group["members"]).forEach(function(member){
                            if(notMatched && member["phoneNumber"] == newRequest["phoneNumber"]) {
                                notMatched = false;
                            }
                        })
                        return notMatched;
                        });
                        
                if(pendingRequests.length < group["pendingAddRequests"].length) {
                    var param2 = {
                    TableName: 'Groups',
                    Key:{
                        "name": group['name']
                    },
                    UpdateExpression: "set pendingAddRequests = :pendingRequests",
                    ExpressionAttributeValues:{
                        ":pendingRequests": pendingRequests
                    },
                    ReturnValues:"UPDATED_NEW"
                };
                
                dynamoDB.update(param2, function(err, data) {});
                }       
            }
        });
        }
    });
};

** UpdateMembers **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {};
    if(event.action == "ADD_FEEDBACK") {
        var feedback = {
            "feedback": event.feedback,
            "createdOn": Date.now()
        }
        param = {
            TableName: 'Members',
            Key:{
                "phoneNumber": event.phoneNumber
            },
            UpdateExpression: "set #feedbacks = list_append(if_not_exists(#feedbacks, :empty_list), :feedback)",
            ExpressionAttributeNames: {
                '#feedbacks' : 'feedbacks'
            },
            ExpressionAttributeValues:{
                ":feedback": [feedback],
                ':empty_list': []
            },
            ReturnValues:"UPDATED_NEW"
        };
        
        dynamoDB.update(param, function(err, data) {
            if (err) {
                callback(err, null);
            } else {
                const response = {
                          statusCode: 200,
                      headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                      },
                    };
                callback(null,response);
            }
        });
    }
}

** addAppMember **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {
        TableName: 'Members',
        KeyConditionExpression: "#phoneNumber = :phoneNumber",
        ExpressionAttributeNames:{
            "#phoneNumber": "phoneNumber"
        },
        ExpressionAttributeValues: {
            ":phoneNumber": event.phoneNumber
        }
    };
    
    dynamoDB.query(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            var response;
            if(data.Count == 0) {
                if(event.nickName && event.nickName.trim().length > 0) {
                    event.createdOn = Date.now();
                   param = {
                        TableName: 'Members',
                        Item:event
                    }; 
                    
                    dynamoDB.put(param, function(err, data) {
                        if (err) {
                            callback(err, null);
                        } else {
                            response = {
                                statusCode: 200,
                                headers: {
                                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                                    "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                                },
                                body: {"Items": [event] }
                            };
                            
                            callback(null, response);
                        }
                    });
                } else {
                    response = {
                      statusCode: 400,
                      headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                      },
                      body: "User is not registered."
                    };
                    
                    callback(null, response);
                }
            } else if(data.Count > 0) {
                if(data["Items"][0]["password"] != event.password) {
                   response = {
                    statusCode: 400,
                    headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                    },
                    body: "User already registered but wrong password provided."
                    };
                    
                    callback(null, response);
                } else {
                   response = {
                      statusCode: 200,
                      headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                      },
                      body: data
                    }; 
                    
                    callback(null, response);
                }
            }
        }
    });
}

** addNewGroup **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {
        TableName: 'Groups',
        KeyConditionExpression: "#name = :name",
        ExpressionAttributeNames:{
            "#name": "name"
        },
        ExpressionAttributeValues: {
            ":name": event.name
        }
    };
    
    dynamoDB.query(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            if(data.Count != 0) {
                callback("Group name already Present.", null);
            } 
            
            event["createdOn"] = Date.now();
            param = {
                TableName: 'Groups',
                Item:event
            }; 
                    
            dynamoDB.put(param, function(err, data) {
                if (err) {
                    callback(err, null);
                } else {
                    var response = {
                        statusCode: 200,
                        headers: {
                            "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                            "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                        }
                    };
                    
                    callback(null, response);
                }
            });
        }
    });    
}

** PopulateLatestQueries **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {
        TableName: 'Groups'
    };
    
    dynamoDB.scan(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            (data["Items"]).forEach(function(group){
                var param2 = {
                    TableName: 'Queries',
                    Limit: 2,
                    ScanIndexForward: false,
                    ConsistentRead: false,
                    FilterExpression: "#groupName = :groupName",
                    ExpressionAttributeNames:{
                        "#groupName": "groupName"
                    },
                    ExpressionAttributeValues: {
                        ":groupName": group["name"]
                    }
                };
                
                dynamoDB.scan(param2, function(err,data){
                    if(err){
                        callback(err, null);
                    }else{
                        if(data.Count > 0) {
                            var param3 = {
                                TableName: 'Groups',
                                Key:{
                                    "name": group['name']
                                },
                                UpdateExpression: "set latestQueries = :latestQueries",
                                ExpressionAttributeValues:{
                                    ":latestQueries": data["Items"]
                                },
                                ReturnValues:"UPDATED_NEW"
                            };
                
                            dynamoDB.update(param3, function(err, data) {});
                        }
                        
                    }
                });
                
            });
        }
    });
}

** GetQuery **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();
const s3 = new AWS.S3();

exports.handler = function(event, context, callback){
    let param;
    
    if(event.imageFileName) {
       param = {
            Bucket: event["imageFileName"].substring(0, event["imageFileName"].indexOf('/')),
            Key: event["imageFileName"].substring(event["imageFileName"].indexOf('/') + 1, event["imageFileName"].length)
        };
        
        s3.getObject(param, function (err, data) {
            if(err) {
                callback(err, null);
            } else {
                callback(null, data.Body.toString('ascii'));
            }
        });
    } else if(event.id) {
        param = {
            TableName: 'Queries',
            KeyConditionExpression: "#id = :id",
            ExpressionAttributeNames:{
                "#id": "id"
            },
            ExpressionAttributeValues: {
                ":id": event.id
            }
        };
        
        dynamoDB.query(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                callback(null, data);
            }
        });
    } else if(event.groupName) {
        param = {
            TableName: 'Queries',
            Limit: 10,
            ScanIndexForward: false,
            ConsistentRead: false,
            IndexName: "groupName-createdOn-index",
            KeyConditionExpression: "#groupName = :groupName",
            ExpressionAttributeNames:{
                "#groupName": "groupName"
            },
            ExpressionAttributeValues: {
                ":groupName": event.groupName
            },
            ExclusiveStartKey: event.exclusiveStartKey
        };
        
        dynamoDB.query(param, function(err,data){
        if(err){
            callback(err, null);
        }else{
            callback(null, data);
        }
    });
    }
}

** SavePaymentDetails **
const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    let param = {};
    var purposeParts = event.purpose.split("_");
    
    if(purposeParts[0] == "PAYFORPOST") {
        var postId = purposeParts[2];
        param = {
            TableName: 'Queries',
            Key:{
                "id": postId
            },
            UpdateExpression: "set #paidPost = :paidPost",
            ExpressionAttributeNames: {
                '#paidPost' : 'paidPost'
            },
            ExpressionAttributeValues:{
                ":paidPost": true,
            },
            ReturnValues:"UPDATED_NEW"
        };
    }
    
    if(purposeParts[0] == "PAYFORGROUP") {
        var phoneNumber = purposeParts[1];
        var groupName = purposeParts[2];
        var nickName = purposeParts[3];
        
        let newMemberRequest =  {
           phoneNumber : phoneNumber,
           nickName: nickName,
           createdOn: Date.now()
        };
        
        param = {
            TableName: 'Groups',
            Key:{
                "name": groupName
            },
            UpdateExpression: "set #members = list_append(if_not_exists(#members, :empty_list), :newMemberRequest)",
            ExpressionAttributeNames: {
                '#members' : 'members'
            },
            ExpressionAttributeValues:{
                ":newMemberRequest": [newMemberRequest],
                ':empty_list': []
            },
            ReturnValues:"UPDATED_NEW"
        };
    }
    
    dynamoDB.update(param, function(err, data) {
    if (err) {
        callback(err, null);
    } else {
        const response = {
                  statusCode: 200,
              headers: {
                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
              },
            };
            callback(null,response);
        }
    });
};

** about **
exports.handler = function(event, context, callback){
    if(event.action == "GET_LATEST_VERSION") {
        callback(null, false)
    }
}
