** Call Shopify API **
const https = require('https');

let options = {
    host : 'area44.myshopify.com',
    path:  '/admin/api/2020-10/products/4730757644351/images.json',
    headers: {
        'cookie': '_master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWxsWldJeU4yRm1aaTAzT0dRekxUUTVOR010WVRBMk1TMHpaV0UwWkRNNU5HUTBPRE1HT2daRlJnPT0iLCJleHAiOiIyMDIyLTA5LTAyVDAyOjUwOjI3LjQ0OVoiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--c364bffaa153ce4ed0771f1cdb206cddc87b36c9; _master_udr=eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaWxsWldJeU4yRm1aaTAzT0dRekxUUTVOR010WVRBMk1TMHpaV0UwWkRNNU5HUTBPRE1HT2daRlJnPT0iLCJleHAiOiIyMDIyLTEwLTI1VDEyOjI4OjAzLjY2MloiLCJwdXIiOiJjb29raWUuX21hc3Rlcl91ZHIifX0%3D--e65d2f7cc22d64be727e8c41bdc4609169275273; new_admin=1; _secure_admin_session_id=9976ea5e5331deddc521245b7a4b76a3; _secure_admin_session_id_csrf=9976ea5e5331deddc521245b7a4b76a3; koa.sid=ZweDTmD42mxwiR_oteZ8k6QUlFxfMKSJ; koa.sid.sig=Ot0KreT4ITLye-mVs_tiNPFTuoU; identity-state=BAh7AA%3D%3D--8efac8b5fc8321eae9cb6794eb3bbe0bf886b891; _ab=1; online_store_editor_user_locale=en-IN; __ssid=ccaa65eb-2ab9-4e70-9d32-78ee2814a92f; _y=58821e03-99de-4a88-b0cc-ed4283909b5a; _orig_referrer=; _landing_page=%2Fadmin%2Fgift_cards; _shopify_y=58821e03-99de-4a88-b0cc-ed4283909b5a; _shopify_fs=2020-10-25T12%3A41%3A16.364Z; _s=61041599-197E-48D4-9D84-45C04AC6E2AE; _shopify_s=61041599-197E-48D4-9D84-45C04AC6E2AE'
        },
};

exports.handler = (event, context, callback) => {
    const req = https.request(options, (res) => {
        let body = '';
        console.log('Status: ', res.statusCode);
        console.log('Headers: ', JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', (chunk) => body += chunk);
        res.on('end', () => {
            console.log('successfully processed HTTPS response');
            console.log(res.headers['content-type']);
            if(res.headers['content-type'] === 'application/json; charset=utf-8') {
                console.log("is json")
                body = JSON.parse(body);
            }
            callback(null, body);
        });
    });
    
    req.on('error', callback);
    req.write(JSON.stringify(event.data));
    req.end();
};