const AWS = require('aws-sdk');
const dynamoDB = new AWS.DynamoDB.DocumentClient();

exports.handler = function(event, context, callback){
    if(event.action == "UPDATE_USER_PROFILE") {
         const param = {
            TableName: 'notes',
            Key:{
                "email": event.email
            },
            UpdateExpression: "SET #key = :value",
            ExpressionAttributeNames: { 
                "#key" : event.key,
            },
            ExpressionAttributeValues: { 
                ":value" : event.value
            },
            ReturnValues:"UPDATED_NEW"
        };
        
        dynamoDB.update(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                var response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                    }
                };
                
                callback(null, response);
            }
        });
    } else if(event.action == "TOGGLE_STRIKE_THROUGH") {
        for(var info of event.actionList){
            const param = {
            TableName: 'notes',
            Key:{
                "email": event.email
            },
            UpdateExpression: "SET notes.#createdOn.#striked = :newStrikeValue",
            ExpressionAttributeNames: { 
                "#createdOn" : info.timestamp,
                "#striked": "striked"
            },
            ExpressionAttributeValues: { 
                ":newStrikeValue" : info.striked
            },
            ReturnValues:"UPDATED_NEW"
        };
        
        dynamoDB.update(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                var response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                    }
                };
                
                callback(null, response);
            }
        });
        }
        
        
    } else if(event.action == "ADD_NEW_NOTES") {
       const param = {
            TableName: 'notes',
            Key:{
                "email": event.email
            },
            UpdateExpression: "SET notes.#createdOn = :notes",
            ExpressionAttributeNames: { "#createdOn" : event.createdOn },
            ExpressionAttributeValues: { 
                ":notes" : event.notes
            },
            ReturnValues:"UPDATED_NEW"
        };
        
        dynamoDB.update(param, function(err,data){
            if(err){
                callback(err, null);
            }else{
                var response = {
                    statusCode: 200,
                    headers: {
                        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                    }
                };
                
                callback(null, response);
            }
        });

    } else if(event.action == "ADD_ROUGHBOOK_PROFILE") {
        let param = {
            TableName: 'notes',
            KeyConditionExpression: "#email = :email",
            ExpressionAttributeNames:{
                "#email": "email"
            },
            ExpressionAttributeValues: {
                ":email": event.member.email
            } 

        }
        
        dynamoDB.query(param, function(err, data) {
            if (err) {
                callback(err, null);
            } else {
                if(data.Count > 0 && event.option == 'Login') {
                    if(data.Items[0].passcode == event.member.passcode) {
                        var response = {
                            statusCode: 200,
                            headers: {
                                "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                                "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                            },
                            data: data.Items[0]
                        };
                            
                        callback(null, response);
                    } else {
                       callback("WRONG PASSWORD.", null); 
                    }
                } else if(data.Count == 0 && event.option == 'Register'){
                    event.member["createdOn"] = Date.now();
                    event.member["notes"] = {};
                    
                    param = {
                        TableName: 'notes',
                        Item: event.member
                    };
        
                    dynamoDB.put(param, function(err, data) {
                        if (err) {
                            callback(err, null);
                        } else {
                            var response = {
                                statusCode: 200,
                                headers: {
                                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                                    "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
                                },
                                data: event.member
                            };
                            callback(null, response);
                        }
                    }); 
                } else if(data.Count > 0 && event.option == 'Register'){
                    callback("USER ALREADY PRESENT, PLEASE LOGIN!", null);
                } else if(data.Count == 0 && event.option == 'Login'){
                    callback("USER NOT PRESENT, PLEASE REGISTER!", null);
                } else {
                    callback("ERROR UNKNOWN", null)
                }
            }
        });
        
         
    } else {
        callback("INVALID ACTION", null);  
    }
}

