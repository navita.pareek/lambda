At Roughbook: Quick Notes Taking & Productivity, we respect the privacy of all our users. This policy describes the types of data we collect and what we do with that data. It is part of our commitment to transparent, responsible, and respectful relationships with users. By agreeing to use our services, you also consent to our Privacy Policy.

What personal data do we collect?
Contact information, such as names and email addresses, allows user to login and check their notes.

How do we collect information?
We collect only user data when you interact with Roughbook, such as when you register to our services.

How do we use personal data?
We collect email address and user name to help us improve and develop our services. This personal information allows us to map user's notes with user. So that when user login again, they will get all their notes back.

To whom do we disclose data?
We do not disclose user's data to anyone. We are not using any third party libraries or services.

What technology do we use to collect data?
We use plain register form to collect user data (user name and email address).

What can users do to limit Roughbook's data collection?
All personal information that product collects helps us provide the best, most personalized experience for our users. Collecting email address is the only way to register. Product can't work without registering, thus all registered user needs to provide this information.

What do we do to protect user data?
We take the security of your personal data seriously. That is why we take comprehensive measures to make sure that all the data we collect is safe and protected. These include using secured-access data centers and frequently monitoring our system for online attacks and system vulnerabilities.
